
// Assignment 3 - Pointers
// <Cael Neary>


#include <iostream>
#include <conio.h>

using namespace std;

// TODO: Implement the "SwapIntegers" function

// Prototype for SwapIntegers function here:
void SwapIntegers(int*, int*);

// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	_getch();
	return 0;
}

void SwapIntegers(int* a, int* b)
{
	int iSwapperoonie = 0;

	iSwapperoonie = *a;
	*a = *b;
	*b = iSwapperoonie;

}
